FROM debian:latest AS base
ARG TARGETPLATFORM

COPY JLink_Linux_V760b_* /tmp

# Update and install necessary packages
RUN apt-get update && apt-get dist-upgrade --yes
RUN apt-get install -y \
make \
gcc-arm-none-eabi \
udev \
libxcb1 \
libxrender1 \
libxcb-render0 \
libxcb-render-util0 \
libxcb-shape0 \
libxcb-randr0 \
libxcb-xfixes0 \
libxcb-sync1 \
libxcb-shm0 \
libxcb-icccm4 \
libxcb-keysyms1 \
libxcb-image0 \
libxkbcommon0 \
libxkbcommon-x11-0 \
libfontconfig1 \
libfreetype6 \
libxext6 \
libx11-6 \
libxcb1 \
libx11-xcb1 \
libsm6 \
libice6 \
libglib2.0-0

# Set the OS version to pull the debian packages from (using a "variable file")
RUN if [ $TARGETPLATFORM = "linux/arm64" ]; then echo "arm64" > arch; elif [ $TARGETPLATFORM = "linux/arm/v7" ]; then echo "arm" > arch; else echo "x86_64" > arch; fi;

# Install the J-link software
RUN dpkg -i /tmp/JLink_Linux_V760b_$(cat arch).deb