# Analog Devices Embedded Build Environment
Containerized environment for building ARM based targets from Analog Devices. This is include the [J-Link Software](https://www.segger.com/downloads/jlink/) for flashing the target devices.

# Building with Docker
To build for multiple targets using Docker build run the following commands:
```
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --use
docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64 -t <IMAGE TAG> .
```

# Running the programmer